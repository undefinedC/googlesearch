import {retrieveSearch} from "../../public/searcher.mjs";
import Cors from 'cors'

const cors = Cors({
    methods: ['POST', 'GET', 'HEAD'],
})

function runMiddleware(req,res,fn) {
    return new Promise((resolve, reject) => {
        fn(req, res, (result) => {
            if (result instanceof Error) {
                return reject(result)
            }
            return resolve(result)
        })
    })
}

export default async (req, res) => {
    await runMiddleware(req, res, cors)
    const {query} = req.query
    const options = {
        page:  req.query["page"] ? Number(req.query["page"]) : 0,
        safe: false,
        parse_ads: false,
        use_mobile_ua: false,
        additional_params: {
            hl: 'en'
        }
    }
    console.log(options)
    const data = await retrieveSearch(query,options)
    return res.status(200).json({data: data});
}