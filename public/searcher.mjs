'use strict';
import * as cheerio from 'cheerio';
import Axios from 'axios';
import {Constants, userAgents} from "./declarations.js";

//const options = {
//    page:  0,
//    safe: false,
//    parse_ads: false,
//    use_mobile_ua: false,
//    additional_params: {
//        hl: 'en'
//    }
//}
//   search("sex",options ).then(x => console.log(x))
export async function retrieveSearch(query, options = {}) {
    const safe = options.safe || false;
    const page = options.page ? options.page * 10 : 0;
    const use_mobile_ua = Reflect.has(options, 'use_mobile_ua') ? options.use_mobile_ua : true;
    const parse_ads = options.parse_ads || false;
    const additional_params = options.additional_params || {};
    const axios_config = options.axios_config || {};
    const _query = query.trim().split(/ +/).join('+');
    const url = encodeURI(`${Constants.URLS.GOOGLE}search?q=${_query}&ie=UTF-8&aomd=1${(safe ? '&safe=active' : '')}&start=${page}`);

    const response = await Axios.get(url, {
        params: additional_params,
        headers: getHeaders({mobile: use_mobile_ua}),
        ...axios_config
    }).catch((err) => err);

    if (response instanceof Error)
        throw new SearchError('AXIOS COULD NOT GET', {
            status_code: response?.status || 0, message: response?.message
        });


    return refineData(response.data, parse_ads, use_mobile_ua)
//
//    return OrganicResults.parse($, parse_ads, use_mobile_ua);
}

class OrganicResult {
    /** @type {string} */
  title;

    /** @type {string} */
  description;

    /** @type {string} */
  url;

    /** @type {boolean} */
  is_sponsored;

    /** @type {{ high_res: string; low_res: string; }} */
  favicons;

    constructor(data) {
        this.title = data.title;
        this.description = data.description;
        this.url = data.url;
        this.is_sponsored = data.is_sponsored;
        this.favicons = data.favicons;
    }
}

class OrganicResults {
    /**
   * @returns {{
   *   title: string;
   *   description: string;
   *   url: string;
   *   is_sponsored: boolean;
   *   favicons: {
   *     high_res: string;
   *     low_res: string;
   *   }
   *  }[]}
   */
  static parse($, parse_ads = false, is_mobile = true) {
        // Stores advert indexes so we can identify them later
        const ad_indexes = [];

        const titles = $(Constants.SELECTORS.TITLE)
      .map((_i, el) => {
          const is_ad =
          el.parent.attribs.style == '-webkit-line-clamp:2' ||
          (!is_mobile && el.parent.attribs.class.startsWith('vdQmEd'));

          // Ignore ad titles if parse_ads is false
          if (!parse_ads && is_ad)
              return null;

          return is_mobile ?
          $(el).text().trim() : $(el).find('h3').text().trim() || $(el).find('a > div > span').first().text().trim();
      }).get();

        const descriptions = $(Constants.SELECTORS.DESCRIPTION)
      .map((_i, el) => {
          const is_ad = el.parent.attribs.class == 'w1C3Le' ||
          (!is_mobile && !Object.keys(el.parent.attribs).length);

          // Ignore ad descriptions if parse_ads is false
          if (!parse_ads && is_ad) {
              return null;
          } else if (is_ad) {
              ad_indexes.push(_i);
          }

          return $(el).text().trim();
      }).get();

        const urls = $(is_mobile ? Constants.SELECTORS.URL : `${Constants.SELECTORS.TITLE} > a`)
      .map((_i, el) => {
          const is_ad = el.parent?.parent?.attribs?.class?.startsWith('vdQmEd');

          /**
         * Since the selector for URLs is the same as the one for titles on desktop,
         * we need to check if the element is an ad. If we're parsing the mobile page,
         * then ads are simply stripped out of the results.
         */
          if (!is_mobile && !parse_ads && is_ad) {
              return null;
          }

          return $(el).attr('href');
      }).get();

        // Refine results
        if (titles.length < urls.length && titles.length < descriptions.length) {
            urls.shift();
        }

        if (urls.length > titles.length) {
            urls.shift();
        }

        const is_innacurate_data = descriptions.length < urls.slice(1).length;

        urls.forEach((item, index) => {
            // Why YouTube? Because video results usually don't have a description.
            if (item.includes('m.youtube.com') && is_innacurate_data) {
                urls.splice(index, 1);
                titles.splice(index, 1);
                index--;
            }
        });

        const results = [];

        for (let i = 0; i < titles.length; i++) {
            const title = titles[i];
            const description = descriptions[i];

            let url = urls[i];

            // Some results have a different URL format (AMP and ad results).
            if (url?.startsWith('/aclk') || url?.startsWith('/amp/s')) {
                url = `${Constants.URLS.W_GOOGLE}${url.substring(1)}`;
            }

            const high_res_favicon = `${Constants.URLS.FAVICONKIT}/${new URL(url || Constants.URLS.W_GOOGLE).hostname}/192`;
            const low_res_favicon = `${Constants.URLS.W_GOOGLE}s2/favicons?sz=64&domain_url=${new URL(url || Constants.URLS.W_GOOGLE).hostname}`;

            if (titles[i] && descriptions[i] && urls[i]) {
                results.push(new OrganicResult({
                    title,
                    description,
                    url,
                    is_sponsored: ad_indexes.includes(i),
                    favicons: {
                        high_res: high_res_favicon,
                        low_res: low_res_favicon
                    }
                }));
            }
        }

        return results;
    }
}

class SearchError extends Error {
    constructor (message, info) {
        super(message);

        info && (this.info = info);

        this.date = new Date();
    }
}

/**
 * Returns headers with a random user agent.
 *
 * @param {boolean} is_mobile
 * @returns {string}
 */
function getHeaders(options = { mobile: false }) {
    const available_agents = userAgents[options.mobile ? 'mobile' : 'desktop'];
    const ua = available_agents[Math.floor(Math.random() * available_agents.length)];

    return {
        'accept': 'text/html',
        'accept-encoding': 'gzip, deflate',
        'accept-language': 'en-US,en',
        'referer': 'https://www.google.com/',
        'upgrade-insecure-requests': 1,
        'user-agent': ua
    };
}

/**
 * Refines the html.
 *
 * @param {string} data - Raw html data.
 * @param {boolean} parse_ads - Whether to parse ads or not.
 * @returns {string}
 */
function refineData (data, parse_ads = false, is_mobile = true) {
    let result = data
    // Removes classes we don't need:
    .replace(/N6jJud MUxGbd lyLwlc/g, '')
    .replace(/YjtGef ExmHv MUxGbd/g, '')
    .replace(/MUxGbd lyLwlc aLF0Z/g, '')

    /*
     * Transforms all possible variations of some classes' name into a
     * fixed string so it's easier to get consistent results:
     **/

    // Descriptions: -> MUxGbd yDYNvb
    .replace(/yDYNvb lEBKkf/g, 'yDYNvb')
    .replace(/VwiC3b MUxGbd yDYNvb/g, 'MUxGbd yDYNvb')

    // Urls: -> C8nzq BmP5tf
    .replace(/cz3goc BmP5tf/g, 'C8nzq BmP5tf')

    // Titles: -> ynAwRc q8U8x MBeuO gsrt oewGkc LeUQr
    .replace(/ynAwRc q8U8x MBeuO oewGkc LeUQr/g, 'ynAwRc q8U8x MBeuO gsrt oewGkc LeUQr')
    .replace(/MBeuO oewGkc/g, 'MBeuO gsrt oewGkc');

    // Transform desktop title/urls classes. Everything else is the same.
    if (!is_mobile) {
        result = result
      .replace(/yuRUbf|v5yQqb/g, 'ynAwRc q8U8x MBeuO gsrt oewGkc LeUQr')
    }

    // Transform ad title classes.
    if (parse_ads) {
        result = result
      .replace(/cz3goc v5yQqb BmP5tf/g, 'C8nzq BmP5tf')
    }

    return result;
}

/**
 * Gets a string between two delimiters.
 *
 * @param {string} data - The data.
 * @param {string} start_string - Start string.
 * @param {string} end_string - End string.
 *
 * @returns {string}
 */

